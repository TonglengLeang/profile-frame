import HomePage from "@/containers/Home";
import styled from "styled-components";

export default function Home() {
  return (
    <Container>
      <HomePage />
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  background-color: #edf8f8;
  font-family: sans-serif;
  color: #000;
`;
