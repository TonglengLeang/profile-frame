import { WrapperLayout } from "@/components/Layout";
import {
  CameraOutlined,
  RotateLeftOutlined,
  RotateRightOutlined,
  ZoomInOutlined,
  ZoomOutOutlined,
} from "@ant-design/icons";
import { Button, Form, Slider } from "antd";
import { useState } from "react";
import AvatarEditor from "react-avatar-editor";
import styled from "styled-components";

const HomePage = () => {
  const [file, setFile] = useState<string>("");
  const handleChange = (e: any) => {
    console.log(e.target.files);
    setFile(URL.createObjectURL(e.target.files[0]));
  };

  const [scaleValue, setScaleValue] = useState(1);
  const [rotationValue, setRotationValue] = useState(0);

  const onSlideToScaleValue = (newValue: number) => {
    setScaleValue(newValue);
  };

  const onSlideToRotateValue = (newValue: number) => {
    setRotationValue(newValue);
  };

  const handleOnScaleDown = () => {
    setScaleValue(scaleValue - 0.2);
  };

  const handleOnScaleUp = () => {
    setScaleValue(scaleValue + 0.2);
  };

  const handleOnRotateLeft = () => {
    setRotationValue(rotationValue - 45);
  };

  const handleOnRotateRight = () => {
    setRotationValue(rotationValue + 45);
  };

  return (
    <WrapperLayout>
      <Container>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            margin: "20px auto",
          }}
        >
          <Preview
            style={{
              position: "absolute",
              zIndex: "2",
            }}
            imageUrl="/static/assets/dino_frame.png"
          ></Preview>
          <AvatarEditor
            style={{
              position: "absolute",
              zIndex: "1",
            }}
            //   image="/static/assets/dino_frame.png"
            image={file?.toString()}
            width={400}
            height={400}
            border={-10}
            color={[255, 255, 255, 0.6]} // RGBA
            scale={scaleValue}
            rotate={rotationValue}
          />
        </div>

        <Form style={{ width: "400px", marginTop: "200px" }}>
          <FormItem
            style={{ display: "flex", flexDirection: "column" }}
            name="scale"
          >
            <CustomButton onClick={handleOnScaleDown}>
              <ZoomOutOutlined />
            </CustomButton>
            <CustomSlider
              min={-20}
              style={{ width: "100%" }}
              defaultValue={1}
              max={22}
              onChange={onSlideToScaleValue}
              value={typeof scaleValue === "number" ? scaleValue : 1.2}
              // tooltip={{ open: true }}
            />
            <CustomButton onClick={handleOnScaleUp}>
              <ZoomInOutlined />
            </CustomButton>
          </FormItem>
          <FormItem
            style={{ display: "flex", flexDirection: "column" }}
            name="rotation"
          >
            <CustomButton onClick={handleOnRotateLeft}>
              <RotateLeftOutlined />
            </CustomButton>
            <CustomSlider
              min={-360}
              style={{ width: "100%" }}
              defaultValue={0}
              max={360}
              onChange={onSlideToRotateValue}
              value={typeof rotationValue === "number" ? rotationValue : 1}
              // tooltip={{ open: true }}
            />
            <CustomButton onClick={handleOnRotateRight}>
              <RotateRightOutlined />
            </CustomButton>
          </FormItem>
        </Form>
        <UploadButton onChange={handleChange}>
          <CameraOutlined style={{ marginRight: "10px" }} />
          Upload Photo
          <input type="file" accept="image/*" required />
        </UploadButton>
      </Container>
    </WrapperLayout>
  );
};
export default HomePage;

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
  height: 100vh;
`;

const UploadButton = styled.label`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 30px;
  width: 400px;
  margin-top: 5px;
  border-radius: 5px;
  background: linear-gradient(235.89deg, #bc0066 11.7%, #560096 96.48%);
  color: #fff;
  cursor: pointer;
  border: 1px solid #ccc;
  padding: 20px;

  input {
    display: none;
    cursor: pointer;
  }
`;

const FormItem = styled(Form.Item)`
  padding: 5px;
  background-color: #fff;
  border: 1px solid #ccc;
  border-radius: 5px;
  .ant-form-item-control-input-content {
    display: flex;
  }
`;

interface PreviewProps {
  imageUrl: string;
}

const Preview = styled.div<PreviewProps>`
  display: flex;
  width: 400px;
  height: 400px;
  /* background-image: linear-gradient(235.89deg, #bc0066 11.7%, #560096 96.48%);
  background-origin: padding-box, border-box;
  background-repeat: no-repeat; */
  border: 5px dotted #ccc;

  background-image: url(${({ imageUrl }) => imageUrl});
  background-repeat: no-repeat;
  background-size: 100% 100%;
`;

const CustomSlider = styled(Slider)`
  &.ant-slider .ant-slider-track {
    background: linear-gradient(233.64deg, #bc0066 12.06%, #560096 119.71%);
  }
`;
const CustomButton = styled(Button)`
  width: 10%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: linear-gradient(235.89deg, #bc0066 11.7%, #560096 96.48%);
  color: white;
  border: none;
  border-radius: 2p;
`;
