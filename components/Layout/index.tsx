import styled from "styled-components";

export const WrapperLayout = styled.div`
  position: relative;
  margin: 0px 20%;
  width: 100%;
  height: 100vh;
`;
