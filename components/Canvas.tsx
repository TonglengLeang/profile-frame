import AvatarEditor from "react-avatar-editor";
import {
  CameraOutlined,
  RotateLeftOutlined,
  RotateRightOutlined,
  ZoomInOutlined,
  ZoomOutOutlined,
} from "@ant-design/icons";
import styled from "styled-components";
import { useState } from "react";
import { Button, Form, Slider } from "antd";

const Canvas = () => {
  const [file, setFile] = useState<string>("");
  const handleChange = (e: any) => {
    console.log(e.target.files);
    setFile(URL.createObjectURL(e.target.files[0]));
  };

  const [scaleValue, setScaleValue] = useState(50);
  const [rotationValue, setRotationValue] = useState(180);

  const grapData = (e: any) => {
    console.log(scaleValue);
  };

  const onScaleValueChange = (e: any) => {
    setScaleValue(e);
  };

  return (
    <Container>
      {/* <Title>Profile Frame Generator</Title> */}

      {/* <ImageFrame imageUrl="/static/assets/dino_frame.png" /> */}

      <div style={{ width: "1000px", height: "100vh" }}>
        <ImageFrame
          imageUrl="/static/assets/dino_frame.png"
          image={file?.toString()}
          // width={400}
          // height={400}
          border={0}
          color={[255, 255, 255, 0.6]} // RGBA
          scale={1.2}
          rotate={0}
        />
      </div>
      {/* 
      <Form onFinish={grapData} style={{ width: "80%" }}>
        <FormItem
          style={{ display: "flex", flexDirection: "column" }}
          name="scale"
        >
          <CustomButton>
            <ZoomOutOutlined />
          </CustomButton>
          <Slider
            min={1}
            style={{ width: "100%" }}
            defaultValue={scaleValue}
            max={100}
            // tooltip={{ open: true }}
          />
          <CustomButton>
            <ZoomInOutlined />
          </CustomButton>
        </FormItem>
        <FormItem
          style={{ display: "flex", flexDirection: "column" }}
          name="rotation"
        >
          <CustomButton>
            <RotateLeftOutlined />
          </CustomButton>
          <Slider
            min={0}
            style={{ width: "100%" }}
            defaultValue={rotationValue}
            max={360}
            // tooltip={{ open: true }}
          />
          <CustomButton>
            <RotateRightOutlined />
          </CustomButton>
        </FormItem>
      </Form> */}
      <UploadButton onChange={handleChange}>
        <CameraOutlined style={{ marginRight: "10px" }} />
        Upload Photo
        <input type="file" accept="image/*" required />
      </UploadButton>
    </Container>
  );
};
export default Canvas;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 500px !important;
  height: 500px !important;
  border-radius: 10px;
  background: #ffffff;
  box-shadow: 0 0 10px 10px rgba(0, 0, 0, 0.1);
  z-index: 1;
`;

const Title = styled.h3``;

interface ImageFrameProps {
  imageUrl: string;
}
const ImageFrame = styled(AvatarEditor)<ImageFrameProps>`
  /* width: 400px;
  height: 400px; */
  /* position: absolute; */
  background-image: url(${({ imageUrl }) => imageUrl});
  background-repeat: no-repeat;
  background-size: 100% 100%;
  position: absolute;
  /* position: 2; */
`;
const UploadButton = styled.label`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 30px;
  width: 80%;
  margin-top: 5px;
  border-radius: 5px;
  background: linear-gradient(235.89deg, #bc0066 11.7%, #560096 96.48%);
  color: #fff;
  cursor: pointer;
  border: 1px solid #ccc;
  padding: 20px;

  input {
    display: none;
    cursor: pointer;
  }
`;

const FormItem = styled(Form.Item)`
  padding: 5px;
  background-color: #fff;
  border: 1px solid #ccc;
  border-radius: 5px;
  .ant-form-item-control-input-content {
    display: flex;
  }
`;

const CustomButton = styled(Button)`
  width: 10%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: linear-gradient(235.89deg, #bc0066 11.7%, #560096 96.48%);
  color: white;
  border: none;
  border-radius: 2p;
`;
